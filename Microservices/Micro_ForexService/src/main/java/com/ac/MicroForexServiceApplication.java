package com.ac;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroForexServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroForexServiceApplication.class, args);
	}

}

