package com.ac;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TempRepository extends JpaRepository<ExchangeValue, Long> {
	
	ExchangeValue findByFromAndTo(String from, String to);
}
