package com.ac;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class MicroCurrencyConversionServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroCurrencyConversionServiceApplication.class, args);
	}

}

